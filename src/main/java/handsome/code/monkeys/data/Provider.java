package handsome.code.monkeys.data;

import java.util.ArrayList;

import lombok.Data;
import lombok.NonNull;

@Data
public class Provider {
	@NonNull
	public String name;

	public ArrayList<Region> regions = new ArrayList<>();

	public int Rv;
}
