package handsome.code.monkeys.data;

import java.util.ArrayList;

import lombok.Data;
import lombok.NonNull;

@Data
public class Region {
	public float cost;

	public ArrayList<Integer> latencies = new ArrayList<>();

	@NonNull
	public String name;

	public int packages;

	public ArrayList<Integer> ui = new ArrayList<>();
}
