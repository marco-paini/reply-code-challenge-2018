package handsome.code.monkeys;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Stream;

import org.fluttercode.datafactory.impl.DataFactory;

import handsome.code.monkeys.data.Project;
import handsome.code.monkeys.data.ProjectSolution;
import handsome.code.monkeys.data.Provider;
import handsome.code.monkeys.data.Region;
import lombok.extern.jbosslog.JBossLog;

@JBossLog
public class HandsomeCodeMonkeys {
	private final static float sNum = (float) Math.pow(10, 9);

	private final ArrayList<String> countries = new ArrayList<>();

	DataFactory df = new DataFactory();

	private int line = 0;

	private int lineProvider = 0;

	private int lineRegion = 0;

	private final String output = System.getProperty("user.dir") + "/../src/main/resources/output/";

	private final ArrayList<Project> projects = new ArrayList<>();

	private Provider provider;

	private final ArrayList<Provider> providers = new ArrayList<>();

	private Region region;

	private final ArrayList<String> services = new ArrayList<>();

	private int V = 0;

	public HandsomeCodeMonkeys(Path file) {
		log.info("=== " + file + " ===");
		try (Stream<String> stream = Files.lines(file)) {
			/* INPUT */
			stream.forEach(string -> {
				String[] strings = string.split(" ");
				if (line == 0) {
					V = Integer.valueOf(strings[0]);
				} else if (line == 1) {
					for (String service : strings) {
						services.add(service);
					}
				} else if (line == 2) {
					for (String country : strings) {
						countries.add(country);
					}
				} else {
					if (lineProvider < V) {
						if (provider == null) {
							provider = new Provider(strings[0]);
							provider.setRv(Integer.valueOf(strings[1]));
						} else {
							switch (lineRegion % 3) {
							case 0:
								region = new Region(strings[0]);
								break;
							case 1:
								region.setPackages(Integer.valueOf(strings[0]));
								region.setCost(Float.valueOf(strings[1]));
								for (int i = 2; i < strings.length; i++) {
									region.getUi().add(Integer.valueOf(strings[i]));
								}
								break;
							default:
								for (String latency : strings) {
									region.getLatencies().add(Integer.valueOf(latency));
								}
								provider.getRegions().add(region);
							}
							lineRegion++;
							if ((lineRegion / 3) == provider.getRv()) {
								lineProvider++;
								lineRegion = 0;
								providers.add(provider);
								provider = null;
							}
						}
					} else {
						Project project = new Project(strings[1], Integer.valueOf(strings[0]));
						for (int i = 2; i < strings.length; i++) {
							project.getUnits().add(Integer.valueOf(strings[i]));
						}
						projects.add(project);
					}
				}
				line++;
			});
			/* INPUT */

			/* CODE */
			Collections.sort(projects, new Comparator<Project>() {
				@Override
				public int compare(Project o1, Project o2) {
					return o2.penality - o1.penality;
				}
			});
			Map<Project, List<ProjectSolution>> map = new HashMap<>();
			for (Project p : projects) {
				map.put(p, new ArrayList<>());
			}
			boolean end = false;
			while (!end) {
				for (Project p : projects) {
					List<ProjectSolution> list = map.get(p);
					int cIndex = countries.indexOf(p.name);
					int rMin = Integer.MAX_VALUE;
					Region region = null;
					Provider providerRes = null;
					for (Provider provider2 : providers) {
						for (Region r : provider2.regions) {
							if (r.latencies.get(cIndex) < rMin && r.packages > 0) {
								rMin = r.latencies.get(cIndex);
								region = r;
								providerRes = provider2;
							}
						}
					}
					if (region == null) {
						end = true;
					} else {
						ProjectSolution ps = new ProjectSolution(providers.indexOf(providerRes),
								providerRes.regions.indexOf(region), 1);
						region.packages--;
						if (list.size() == 0) {
							list.add(ps);
						} else {
							boolean found = false;
							for (ProjectSolution l : list) {
								if (ps.equals(l)) {
									l.nPackage++;
									found = true;
									break;
								}
							}
							if (!found) {
								list.add(ps);
							}
						}
					}
				}
			}
			boolean run = true;
			float currentSolution = getSolutionRank(map);
			while (run) {
				int guessIndexRemove = guessBest(map);
				List<ProjectSolution> listRemove = map.get(projects.get(guessIndexRemove));
				ProjectSolution psRemove = null;
				ProjectSolution psAdd = null;

				do {
					listRemove = map.get(projects.get(guessIndexRemove));
					if (listRemove.size() != 0)
						psRemove = listRemove.get(0);
				} while (psRemove == null);
				int guessIndexAdd = guessWorst(map);
				List<ProjectSolution> listAdd = map.get(projects.get(guessIndexAdd));
				if (listAdd.contains(psRemove)) {
					for (ProjectSolution projectSolution : listAdd) {
						if (projectSolution.equals(psRemove)) {
							psAdd = projectSolution;
							psAdd.nPackage++;
							psRemove.nPackage--;
							break;
						}
					}
				} else {
					psAdd = new ProjectSolution(psRemove.providerIndex, psRemove.regionIndex, 1);
					listAdd.add(psAdd);
					psRemove.nPackage--;
				}
				float newSolution = getSolutionRank(map);
				if (newSolution > currentSolution) {
					currentSolution = newSolution;
					log.info("a new solution");
					printSolutionOnConsole(file.getFileName().toString(), map);
					return;
				} else {
					if (psAdd.nPackage == 1) {
						listAdd.remove(psAdd);
					} else {
						for (ProjectSolution projectSolution : listAdd) {
							if (projectSolution.equals(psAdd)) {
								psAdd.nPackage--;
								break;
							}
						}
					}
					psRemove.nPackage++;
				}
			}
			/* CODE */

			/* OUTPUT */
			printSolutionOnFile(file.getFileName().toString(), map);
			/* OUTPUT */
		} catch (IOException e) {
			log.error(e);
		}
	}

	private float computeApl(Project project, List<ProjectSolution> solutions) {
		float num = 0;
		float den = 0;
		for (ProjectSolution ps : solutions) {
			Region curRegion = getCurrentRegion(ps);
			float lr = curRegion.getLatencies().get(countries.indexOf(project.getName()));
			float ur = curRegion.getUi().stream().reduce(0, (a, b) -> a + b) * ps.nPackage;
			num += lr * ur;
			den += ur;
		}
		return den == 0 ? 0 : num / den;
	}

	private float computeFp(Project project, List<ProjectSolution> solutions) {
		List<Float> allocated = new ArrayList<>(services.size());
		initArr(allocated, services.size());
		for (ProjectSolution ps : solutions) {
			Region curRegion = getCurrentRegion(ps);
			for (int i = 0; i < curRegion.getUi().size(); i++) {
				allocated.set(i,
						(allocated.get(i) == null ? 0 : allocated.get(i)) + (curRegion.getUi().get(i) * ps.nPackage));
			}
		}
		float sumF = 0;
		for (int i = 0; i < services.size(); i++) {
			sumF += project.getPenality()
					* ((project.getUnits().get(i) - Math.min(project.getUnits().get(i), allocated.get(i)))
							/ project.getUnits().get(i));
		}
		return sumF / services.size();
	}

	private float computeOai(Project project, List<ProjectSolution> solutions) {
		List<Float> aNum = new ArrayList<>();
		initArr(aNum, services.size());
		List<Float> aDen = new ArrayList<>(services.size());
		initArr(aDen, services.size());
		for (ProjectSolution ps : solutions) {
			Region curRegion = getCurrentRegion(ps);
			for (int i = 0; i < curRegion.getUi().size(); i++) {
				float q = curRegion.getUi().get(i) * ps.nPackage;
				aNum.set(i, (aNum.get(i) == null ? 0 : aNum.get(i)) + q);
				aDen.set(i, (aDen.get(i) == null ? 0 : aDen.get(i)) + (float) Math.pow(q, 2));
			}
		}
		float res = 0;
		for (int i = 0; i < services.size(); i++) {
			if (aDen.get(i) != 0) {
				res += (float) Math.pow(aNum.get(i), 2) / aDen.get(i);
			}
		}
		return res / services.size();
	}

	private float computeOpc(Project project, List<ProjectSolution> solutions) {
		float opc = 0;
		for (ProjectSolution ps : solutions) {
			Region curRegion = getCurrentRegion(ps);
			opc = curRegion.getCost() * ps.nPackage;
		}
		return opc;
	}

	private Region getCurrentRegion(ProjectSolution ps) {
		return providers.get(ps.providerIndex).getRegions().get(ps.regionIndex);
	}

	private float getSolutionRank(Map<Project, List<ProjectSolution>> map) {
		float score = 0;
		for (Entry<Project, List<ProjectSolution>> project : map.entrySet()) {
			score += getSolutionRank(project.getKey(), project.getValue());
		}
		return score;
	}

	private float getSolutionRank(Project project, List<ProjectSolution> solutions) {
		float apl = computeApl(project, solutions);
		float oai = computeOai(project, solutions);
		float opc = computeOpc(project, solutions);
		float tp = apl * opc / oai;
		float fp = computeFp(project, solutions);
		float f = sNum / (tp + fp);
		return f;
	}

	private int guessBest(Map<Project, List<ProjectSolution>> map) {
		return df.getNumberBetween(0, map.size());
	}

	private int guessWorst(Map<Project, List<ProjectSolution>> map) {
		return df.getNumberBetween(0, map.size());
	}

	private void initArr(List<Float> aNum, int size) {
		for (int i = 0; i < size; i++) {
			aNum.add(i, 0f);
		}
	}

	private void printSolutionOnConsole(String suffix, Map<Project, List<ProjectSolution>> map) throws IOException {
		final StringBuilder stringBuilder = new StringBuilder();
		map.forEach((project, projectSolutions) -> {
			projectSolutions.forEach(projectSolution -> {
				stringBuilder.append(projectSolution.providerIndex + " " + projectSolution.regionIndex + " "
						+ projectSolution.nPackage + " ");
			});
			stringBuilder.append("\n");
		});
		log.info(stringBuilder);
	}

	private void printSolutionOnFile(String suffix, Map<Project, List<ProjectSolution>> map) throws IOException {
		final StringBuilder stringBuilder = new StringBuilder();
		map.forEach((project, projectSolutions) -> {
			projectSolutions.forEach(projectSolution -> {
				stringBuilder.append(projectSolution.providerIndex + " " + projectSolution.regionIndex + " "
						+ projectSolution.nPackage + " ");
			});
			stringBuilder.append("\n");
		});
		Files.write(Paths.get(output + suffix), stringBuilder.toString().getBytes());
	}
}
